# frozen_string_literal: true

# See http://doc.gitlab.com/ce/development/migration_style_guide.html
# for more information on how to write migrations for GitLab.

class IdentityAccessControl < ActiveRecord::Migration[5.2]
  include Gitlab::Database::MigrationHelpers

  # Set this constant to true if this migration requires downtime.
  DOWNTIME = false

  def change
    # rubocop:disable Migration/AddLimitToStringColumns
    add_column :identities, :ial, :integer
    add_column :identities, :aal, :integer
    add_column :users, :current_sign_in_source, :string
    # rubocop:enable Migration/AddLimitToStringColumns
  end
end
