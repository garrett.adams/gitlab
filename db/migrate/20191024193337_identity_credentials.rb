# frozen_string_literal: true

# See http://doc.gitlab.com/ce/development/migration_style_guide.html
# for more information on how to write migrations for GitLab.

class IdentityCredentials < ActiveRecord::Migration[5.2]
  include Gitlab::Database::MigrationHelpers

  # Set this constant to true if this migration requires downtime.
  DOWNTIME = false

  def change
    # rubocop:disable Migration/AddLimitToStringColumns
    add_column :identities, :token, :string
    add_column :identities, :expires, :boolean
    add_column :identities, :expires_at, :string
    add_column :identities, :refresh_token, :string
    # rubocop:enable Migration/AddLimitToStringColumns
  end
end
