# frozen_string_literal: true

# See http://doc.gitlab.com/ce/development/migration_style_guide.html
# for more information on how to write migrations for GitLab.

class FederatedRunner < ActiveRecord::Migration[5.2]
  include Gitlab::Database::MigrationHelpers

  # Set this constant to true if this migration requires downtime.
  DOWNTIME = false

  def change
    # rubocop:disable Migration/AddLimitToStringColumns
    add_column :ci_runners, :federated, :boolean
    add_column :ci_runners, :auth_source, :string
    add_column :ci_runners, :min_ial, :integer
    add_column :ci_runners, :min_aal, :integer
    # rubocop:enable Migration/AddLimitToStringColumns
  end
end
