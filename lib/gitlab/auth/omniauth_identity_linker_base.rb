# frozen_string_literal: true

module Gitlab
  module Auth
    class OmniauthIdentityLinkerBase
      attr_reader :current_user, :oauth, :session

      def initialize(current_user, oauth, session = {})
        @current_user = current_user
        @oauth = oauth
        @changed = false
        @updated = false
        @session = session
      end

      def link
        if unlinked?
          save
        elsif updatable?
          update
        end
      end

      def changed?
        @changed
      end

      def updated?
        @updated
      end

      def failed?
        error_message.present?
      end

      def error_message
        identity.validate

        identity.errors.full_messages.join(', ')
      end

      private

      def save
        set_credentials
        @changed = identity.save
        expire_credentials
      end

      def update
        set_credentials
        @updated = identity.save
        expire_credentials
      end

      def unlinked?
        identity.new_record?
      end

      def updatable?
        identity.expires
      end

      # rubocop: disable CodeReuse/ActiveRecord
      def identity
        @identity ||= current_user.identities
                                  .with_extern_uid(provider, uid)
                                  .first_or_initialize(extern_uid: uid)
      end
      # rubocop: enable CodeReuse/ActiveRecord

      def provider
        oauth['provider']
      end

      def uid
        oauth['uid']
      end

      def set_credentials
        identity.token = oauth.dig(:credentials, :token)
        identity.expires = oauth.dig(:credentials, :expires)
        identity.expires_at = oauth.dig(:credentials, :expires_at)
        identity.refresh_token = oauth.dig(:credentials, :refresh_token)
        identity.ial = oauth.dig(:credentials, :IAL)
        identity.aal = oauth.dig(:credentials, :AAL)
      end

      def expire_credentials
        return unless identity.expires

        OmniauthWorker.perform_at(identity.expires_at, @current_user.id, provider)
      end
    end
  end
end
