# frozen_string_literal: true

module Gitlab
  class FederationLogger < Gitlab::JsonLogger
    def self.info(event)
      message = {:uuid => event.uuid, :event_type => event.event_type, :details => event.details}
      build.info(message)
    end

    def self.file_name_noext
      'federation'
    end
  end
end
