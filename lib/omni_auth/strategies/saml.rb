# frozen_string_literal: true

module OmniAuth
  module Strategies
    class SAML
      extend ::Gitlab::Utils::Override

      # NOTE: This method duplicates code from omniauth-saml
      #       so that we can access authn_request to store it
      #       See: https://github.com/omniauth/omniauth-saml/issues/172
      override :request_phase
      def request_phase
        authn_request = OneLogin::RubySaml::Authrequest.new

        store_authn_request_id(authn_request)

        with_settings do |settings|
          redirect(authn_request.create(settings, additional_params_for_authn_request))
        end
      end

      credentials do
        expires_at = @response_object.session_expires_at
        expires_at = expires_at.to_time unless expires_at.nil?
        expires_at ||= @response_object.not_on_or_after
        expires_at = expires_at.to_i unless expires_at.nil?
        expires = expires_at.nil? ? false : true
        token = @session_index if expires

        { token: token, expires: expires, expires_at: expires_at }
      end

      private

      def store_authn_request_id(authn_request)
        Gitlab::Auth::Saml::OriginValidator.new(session).store_origin(authn_request)
      end
    end
  end
end
