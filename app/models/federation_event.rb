# frozen_string_literal: true

require 'securerandom'

class FederationEvent                    
  include ActiveModel::Model
  include ActiveModel::Serialization

  attr_accessor :uuid, :event_type, :details
  validates_presence_of :uuid, :event_type

  def initialize(attributes = {})
    super
    @uuid = SecureRandom.uuid
    @event_type = :generic
  end

  def generic()
    @event_type = :generic
    self 
  end

  def pipeline_triggered()
    @event_type = :pipeline_triggered
    self
  end

  def omniauth()
    @event_type = :omniauth
    self
  end

end

