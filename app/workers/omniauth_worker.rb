# frozen_string_literal: true

class OmniauthWorker
  include ApplicationWorker

  queue_namespace :federation
  feature_category :authentication_and_authorization

  attr_accessor :logger

  def initialize
    @logger = Gitlab::FederationLogger.build
  end

  def perform(user_id, provider)
    identity = User.find_by_id(user_id).find_identity(provider)
    return unless expired_token?(identity) || !valid_token?(identity)

    refresh_token(identity)
  end

  private

  def expired_token?(identity)
    # Sidekiq polls for jobs every 5 seconds by default since v5.1 (15 seconds before that)
    # Set the expiration time to be 6 seconds earlier in case the job triggers too early
    # Note that config/initializers/sidekiq.rb does not set average_scheduled_poll_interval
    # offset = Sidekiq.configure_server.average_scheduled_poll_interval + 1
    offset = 6
    exp_time = identity.expires_at.to_i - offset
    if exp_time < Time.now.to_i
      identity.token = ""
      identity.save
      return true
    else
      return false
    end
  end

  def valid_token?(identity)
    token = identity.token
    return false if token.nil? || token == ""

    provider = identity.provider
    # TODO: Hit validation endpoint of provider
    response = Net::HTTPResponse.new(1.0, "200", "OK")
    if response.code == "200"
      return true
    else
      logger.info(FederationEvent.new(:details => self.access_fail).omniauth)
      identity.token = ""
      identity.save
      return false
    end
  end

  def refresh_token(identity)
    refresh_token = identity.refresh_token
    return if refresh_token.nil? || refresh_token == ""

    provider = identity.provider
    # TODO: Hit refresh endpoint of provider
    response = Net::HTTPResponse.new(1.0, "404", "OK")
    if response.code == "200"
      # identity.token = response.token
      # identity.expires_at = response.expires_at
      # identity.save
      logger.info(FederationEvent.new(:details => self.refreshed_token).omniauth)
    else
      logger.info(FederationEvent.new(:details => self.refresh_fail).omniauth)
    end
  end
  
  def refreshed_token
    {message: "Refreshed #{identity.extern_uid}'s access token from #{provider}"}
  end

  def refresh_fail
    {message: "Failed to refresh #{identity.extern_uid}'s access token from #{provider}"}
  end

  def access_fail
    {message: "Failed to validate #{identity.extern_uid}'s access token with #{provider}"}
  end

end
