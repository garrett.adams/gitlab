# frozen_string_literal: true

class IdentityProviderPolicy < BasePolicy
  # protected_providers = %w(saml cas3)
  # desc "Provider is SAML or CAS3"
  protected_providers = []
  desc "XXX Removing protected providers for now"
  condition(:protected_provider, scope: :subject, score: 0) { protected_providers.include?(@subject.to_s) }

  rule { anonymous }.prevent_all

  rule { default }.policy do
    enable :unlink
    enable :link
  end

  rule { protected_provider }.prevent(:unlink)
end

IdentityProviderPolicy.prepend_if_ee('EE::IdentityProviderPolicy')
