# frozen_string_literal: true

module Federation
  class Lookup
    def self.federated_username(user,runner)
      provider = runner.auth_id.to_s if runner && runner.federated
      identity = user.find_identity(provider)
      unless identity.nil?
        identity.extern_uid
      end
    end

    def self.auth_token(user,runner)
      provider = runner.auth_id.to_s if runner && runner.federated
      identity = user.find_identity(provider)
      unless identity.nil?
        identity.token
      end
    end

  end
end


