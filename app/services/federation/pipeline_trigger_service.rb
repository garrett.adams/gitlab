# frozen_string_literal: true
require 'securerandom'

module Federation
  class PipelineTriggerService

    @build = {}

    def initialize(build = {})
      @build = build
      event = self.from_build  
      Gitlab::FederationLogger.info(event)
      log_to_audit(event)
    end

    def from_build
      FederationEvent.new(:details => self.details).pipeline_triggered
    end

    def log_to_audit(event)
      user = User.find_by(username: event.details[:gitlab_user])
      project = Project.find_by(name: event.details[:project])
      AuditEventService.new(user, project, {:action => :custom, :custom_message => "Federation Event: #{event.uuid}"}).for_project.security_event
    end

    def details
      {:gitlab_user => self.username, :federated_username => self.federated_username, :project => self.project, :commit_hash => self.commit_hash}
    end

    def username
      User.find(@build.user_id).username
    end

    def federated_username
      Federation::Lookup.federated_username(@build.user, @build.runner)
    end

    def project
      @build.project.name
    end

    def commit_hash 
      @build.project.commit.short_id
    end

  end
end
